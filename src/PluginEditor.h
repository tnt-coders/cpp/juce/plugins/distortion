#pragma once

#include "PluginProcessor.h"

class PluginEditor : public juce::AudioProcessorEditor
{
public:
    explicit PluginEditor(PluginProcessor& processor, juce::AudioProcessorValueTreeState& parameters);
    ~PluginEditor() override;

    void paint (juce::Graphics&) override;
    void resized() override;

private:
    PluginProcessor& processor;
    juce::AudioProcessorValueTreeState& parameters;
    juce::Label driveLabel;
    juce::Slider driveSlider;
    juce::AudioProcessorValueTreeState::SliderAttachment driveSliderAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginEditor)
};
