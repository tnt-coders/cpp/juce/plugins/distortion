#include "PluginProcessor.h"

#include "PluginEditor.h"

#include <limits>
#include <memory>

PluginProcessor::PluginProcessor()
    : AudioProcessor(BusesProperties()
#if !JucePlugin_IsMidiEffect
#    if !JucePlugin_IsSynth
                         .withInput("Input", juce::AudioChannelSet::stereo(), true)
#    endif
                         .withOutput("Output", juce::AudioChannelSet::stereo(), true)
#endif
                         )
    , parameters(*this,
                 nullptr,
                 "Distortion",
                 {std::make_unique<juce::AudioParameterFloat>("drive", "Drive", 0.f, 10.f, 5.f)})
    , oversampling(getMainBusNumInputChannels(),
                   4,
                   juce::dsp::Oversampling<float>::filterHalfBandFIREquiripple)
{}

PluginProcessor::~PluginProcessor() = default;

const juce::String PluginProcessor::getName() const
{
    return JucePlugin_Name;
}

bool PluginProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool PluginProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool PluginProcessor::isMidiEffect() const
{
#if JucePlugin_IsMidiEffect
    return true;
#else
    return false;
#endif
}

double PluginProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int PluginProcessor::getNumPrograms()
{
    return 1;
}

int PluginProcessor::getCurrentProgram()
{
    return 0;
}

void PluginProcessor::setCurrentProgram(int index)
{
    juce::ignoreUnused(index);
}

const juce::String PluginProcessor::getProgramName(int index)
{
    juce::ignoreUnused(index);
    return {};
}

void PluginProcessor::changeProgramName(int index, const juce::String& newName)
{
    juce::ignoreUnused(index, newName);
}

void PluginProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    juce::ignoreUnused(sampleRate, samplesPerBlock);

    juce::dsp::ProcessSpec processSpec{};
    processSpec.sampleRate       = sampleRate;
    processSpec.maximumBlockSize = samplesPerBlock;
    processSpec.numChannels      = getMainBusNumInputChannels();

    highPassFilter.state = juce::dsp::IIR::Coefficients<float>::makeFirstOrderHighPass(
        processSpec.sampleRate,
        20.f);
    highPassFilter.prepare(processSpec);

    drive.prepare(processSpec);

    oversampling.initProcessing(samplesPerBlock);

    jassert(oversampling.getOversamplingFactor() < std::numeric_limits<int>::max());
    int oversamplingFactor = static_cast<int>(oversampling.getOversamplingFactor());

    juce::dsp::ProcessSpec oversampledProcessSpec{};
    oversampledProcessSpec.sampleRate       = sampleRate * oversamplingFactor;
    oversampledProcessSpec.maximumBlockSize = samplesPerBlock * oversamplingFactor;
    oversampledProcessSpec.numChannels      = getMainBusNumInputChannels();

    waveShaper.functionToUse = std::tanh;
    waveShaper.prepare(oversampledProcessSpec);

    lowPassFilter.state = juce::dsp::IIR::Coefficients<float>::makeFirstOrderLowPass(
        processSpec.sampleRate,
        20000.f);
    lowPassFilter.prepare(processSpec);
}

void PluginProcessor::releaseResources() {}

bool PluginProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
#if JucePlugin_IsMidiEffect
    juce::ignoreUnused(layouts);
    return true;
#else
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
        && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

#    if !JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
#    endif

    return true;
#endif
}

void PluginProcessor::processBlock(juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ignoreUnused(midiMessages);
    juce::ScopedNoDenormals noDenormals;

    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    jassert(totalNumInputChannels == totalNumOutputChannels);

    // Initialize values
    drive.setGainDecibels(parameters.getRawParameterValue("drive")->load() * 5.f);

    // Create a process context from the AudioBuffer
    juce::dsp::AudioBlock<float>              block(buffer);
    juce::dsp::ProcessContextReplacing<float> context(block);

    // Apply high pass filter
    highPassFilter.process(context);

    // Add gain to the signal to cause it to distort
    drive.process(context);

    // Create an oversampled process context to avoid aliasing when the wave shaper is applied
    auto oversampledBlock = oversampling.processSamplesUp(context.getInputBlock());
    juce::dsp::ProcessContextReplacing<float> oversampledContext(oversampledBlock);

    // Apply distortion
    waveShaper.process(oversampledContext);

    // Downsample (this seems to apply its own low pass filter to avoid aliasing)
    oversampling.processSamplesDown(context.getOutputBlock());

    // Apply low pass filter
    lowPassFilter.process(context);
}

bool PluginProcessor::hasEditor() const
{
    return true;
}

juce::AudioProcessorEditor* PluginProcessor::createEditor()
{
    return new PluginEditor(*this, parameters);
}

void PluginProcessor::getStateInformation(juce::MemoryBlock& destData)
{
    juce::MemoryOutputStream outputStream(destData, false);
    parameters.state.writeToStream(outputStream);
}

void PluginProcessor::setStateInformation(const void* data, int sizeInBytes)
{
    auto state = juce::ValueTree::readFromData(data, sizeInBytes);
    if (state.isValid())
    {
        parameters.replaceState(state);
    }
}

juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new PluginProcessor();
}
