#include "PluginEditor.h"

PluginEditor::PluginEditor(PluginProcessor& processor, juce::AudioProcessorValueTreeState& parameters)
    : AudioProcessorEditor(&processor)
    , processor(processor)
    , parameters(parameters)
    , driveSliderAttachment(parameters, "drive", driveSlider)
{
    addAndMakeVisible(&driveLabel);
    driveLabel.setJustificationType(juce::Justification::centred);
    driveLabel.setText("Drive", juce::dontSendNotification);

    addAndMakeVisible(&driveSlider);
    driveSlider.setSliderStyle(juce::Slider::Rotary);
    driveSlider.setTextBoxStyle(juce::Slider::TextBoxBelow, false, 50, 20);

    setSize(200, 200);
}

PluginEditor::~PluginEditor() = default;

void PluginEditor::paint(juce::Graphics& g)
{
    g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
}

void PluginEditor::resized()
{
    juce::FlexBox flexBox;
    flexBox.alignContent = juce::FlexBox::AlignContent::center;
    flexBox.flexDirection = juce::FlexBox::Direction::column;
    flexBox.justifyContent = juce::FlexBox::JustifyContent::center;

    flexBox.items.add(
        juce::FlexItem(50, 20, driveLabel),
        juce::FlexItem(100, 100, driveSlider));

    flexBox.performLayout(getLocalBounds());
}
